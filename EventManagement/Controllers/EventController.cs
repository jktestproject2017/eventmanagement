﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventManagement;
using EventManagement.Models;
using EventManagement.DAL;
using System;
using Microsoft.AspNet.Identity;

namespace EventManagement.Controllers
{
    public class EventController : Controller
    {
        private EventManagementSystemDBEntities db = new EventManagementSystemDBEntities();
        private EventManagerDB eventManager = new EventManagerDB();

        #region Event
        public ActionResult Events()
        {
            return View(eventManager.AllPublicEvents());
        }
        [Authorize]
        public ActionResult AllEvents()
        {
            return View(eventManager.AllEvents());
        }

        [Authorize]
        public ActionResult EventByUser()
        {
            string id = User.Identity.GetUserId();
            return View(eventManager.GetEventsByUserId(id));
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,StartDate,EndDate,Description,IsPrivate")] EventDto eventDto)
        {
            string userId = User.Identity.GetUserId();
            eventDto.AuthorId = userId;

            TimeSpan tempDuration = eventDto.EndDate.Subtract(eventDto.StartDate);
            eventDto.Duration = tempDuration.Days + "Days- " + tempDuration.Hours + " Hours- " + tempDuration.Minutes + " Minutes ";

            try
            {
                if (ModelState.IsValid)
                {

                    bool eventIsAdded = eventManager.AddEvent(eventDto);
                    if (eventIsAdded)
                        ViewBag.EventAddedStatus = "true";
                    else
                        ViewBag.EventAddedStatus = "false";


                    ModelState.Clear();
                    return View(new EventDto());
                }
                else
                {
                    return View(eventDto);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult EventDetailsById(int id)
        {
            try
            {
                if (id > 0)
                {
                    EventDto eventDetail = eventManager.GetEventDetailById(id);
                    return PartialView("_EventDetailsById", eventDetail);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
        public string DeleteEvent(int id)
        {
            if (id != null)
            {
                if (id > 0)
                {
                    bool isDeleted = eventManager.DeleteEventById(id);
                    if (isDeleted)
                        return "true";
                    else
                        return "false";
                }
            }
            return "false";
        }
        public ActionResult EditEvent(int id)
        {
            if (id > 0)
            {
                EventDto evt = eventManager.GetEventDetailById(id);
                return View(evt);
            }
            return null;
        }
        [HttpPost]
        public ActionResult EditEvent(EventDto eventDto)
        {
            string userId = User.Identity.GetUserId();
            eventDto.AuthorId = userId;

            TimeSpan tempDuration = eventDto.EndDate.Subtract(eventDto.StartDate);
            eventDto.Duration = tempDuration.Days + "Days- " + tempDuration.Hours + " Hours- " + tempDuration.Minutes + " Minutes ";

            try
            {
                if (ModelState.IsValid)
                {

                    bool isUpdated = eventManager.UpdateEventDetails(eventDto);

                    if (isUpdated)
                        ViewBag.EventAddedStatus = "true";
                    else
                        ViewBag.EventAddedStatus = "false";


                    ModelState.Clear();
                    return View(new EventDto());
                }
                else
                {
                    return View(eventDto);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Comments
        public ActionResult EventComments(int id)
        {
            ViewBag.EventId = id;

            List<EventCommentDto> eventComments = eventManager.EventComments(id);
            return PartialView("_EventComments",eventComments);
        }
        public string AddComment(CommentDto model, int eventId)
        {
            model.datetime = DateTime.Now;
            bool status = eventManager.AddComment(model, eventId);
            if(status)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }

        #endregion

        #region Default
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
