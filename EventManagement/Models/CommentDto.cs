﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class CommentDto
    {
        public CommentDto()
        {
            this.EventComments = new HashSet<EventCommentDto>();
        }
    
        public int Id { get; set; }
        public string CommentContent { get; set; }
        public System.DateTime datetime { get; set; }
        public virtual ICollection<EventCommentDto> EventComments { get; set; }
    }
}