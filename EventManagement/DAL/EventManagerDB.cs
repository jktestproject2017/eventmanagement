﻿using EventManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
namespace EventManagement.DAL
{
    public class EventManagerDB
    {

        #region Event
        public List<EventDto> AllEvents()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Event, EventDto>();
                cfg.CreateMap<EventDto, Event>();
            });

            using (var context = new EventManagementSystemDBEntities())
            {
                List<Event> events = context.Events.Include("AspNetUser").ToList();
                List<EventDto> result = Mapper.Map<List<EventDto>>(events);
                return result;
            }
        }
        public List<EventDto> AllPublicEvents()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Event, EventDto>();
                cfg.CreateMap<EventDto, Event>();
            });

            using (var context = new EventManagementSystemDBEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                List<Event> events = context.Events.Where(x => x.IsPrivate == false).OrderByDescending(y => y.StartDate).ToList();
                List<EventDto> result = Mapper.Map<List<EventDto>>(events);
                return result;
            }
        }
        public bool AddEvent(EventDto model)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Event, EventDto>();
                cfg.CreateMap<EventDto, Event>();
            });

            Event newEvent = Mapper.Map<Event>(model);
            using (var context = new EventManagementSystemDBEntities())
            {
                context.Events.Add(newEvent);
                int count = context.SaveChanges();

                if (count > 0)
                    return true;

                else
                    return false;
            }
        }
        public EventDto GetEventDetailById(int id)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Event, EventDto>();
                cfg.CreateMap<EventDto, Event>();
            });

            EventDto eventDetail = new EventDto();
            using (var context = new EventManagementSystemDBEntities())
            {
                Event resultDto = context.Events.Where(x => x.Id == id).FirstOrDefault();
                eventDetail = Mapper.Map<EventDto>(resultDto);
            }
            return eventDetail;
        }
        public List<EventDto> GetEventsByUserId(string id)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Event, EventDto>();
                cfg.CreateMap<EventDto, Event>();
            });

            using (var context = new EventManagementSystemDBEntities())
            {
                List<Event> events = context.Events.Where(x => x.AuthorId == id).OrderByDescending(y => y.StartDate).ToList();
                List<EventDto> result = Mapper.Map<List<EventDto>>(events);
                return result;
            }
        }
        public bool DeleteEventById(int id)
        {
            using (var context = new EventManagementSystemDBEntities())
            {
                Event evt = context.Events.Where(x => x.Id == id).FirstOrDefault();
                context.Entry(evt).State = System.Data.Entity.EntityState.Deleted;
                int count = context.SaveChanges();
                if (count > 0)
                    return true;
                else
                    return false;
            }
        }
        public bool UpdateEventDetails(EventDto model)
        {
            using (var context = new EventManagementSystemDBEntities())
            {
                Event evt = context.Events.Where(x => x.Id == model.Id).FirstOrDefault();
                evt.Title = model.Title;
                evt.StartDate = model.StartDate;
                evt.EndDate = model.EndDate;
                evt.Duration = model.Duration;
                evt.IsPrivate = model.IsPrivate;
                evt.Description = model.Description;

                context.Entry(evt).State = System.Data.Entity.EntityState.Modified;
                int count = context.SaveChanges();
                if (count > 0)
                    return true;
                else
                    return false;
            }
        }
        #endregion

        #region Comments
        public bool AddComment(CommentDto model, int eventId)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Comment, CommentDto>();
                cfg.CreateMap<CommentDto, Comment>();
            });

            Comment newCmt = Mapper.Map<Comment>(model);

            using (var context = new EventManagementSystemDBEntities())
            {
                context.Comments.Add(newCmt);
                int count = context.SaveChanges();
                int commentId = newCmt.Id;

                if (count > 0)
                {
                    EventComment newEventCmt = new EventComment();
                    newEventCmt.EventId = eventId;
                    newEventCmt.CommentId = commentId;

                    context.EventComments.Add(newEventCmt);
                    context.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
        }
        public List<EventCommentDto> EventComments(int id)
        {
            using (var context = new EventManagementSystemDBEntities())
            {
                List<EventCommentDto> eventCmt1 = context.EventComments.Include("Event").Include("Comment").Where(x => x.EventId == id).EventCommentMap().ToList();
             // var eventCmt = context.Events.Include("EventComment.Comment").Where(x => x.Id == id).ToList();
                return eventCmt1;
            }
        }
        #endregion


    }
}