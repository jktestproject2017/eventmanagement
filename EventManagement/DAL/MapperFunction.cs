﻿using EventManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.DAL
{
    public static class MapperFunction
    {
        public static IQueryable<EventCommentDto> EventCommentMap(this IQueryable<EventComment> entity)
        {
            return entity.Select(m => new EventCommentDto
            {
                Id = m.Id,
                EventId = m.EventId,
                CommentId = m.CommentId,
                Event = new EventDto
                {
                    Id = m.Event.Id,
                    Title = m.Event.Title,
                    StartDate = m.Event.StartDate,
                    EndDate = m.Event.EndDate,
                    Description = m.Event.Description,
                    Duration = m.Event.Duration,
                    IsPrivate = m.Event.IsPrivate,
                    AuthorId = m.Event.AuthorId
                },
                Comment = new CommentDto
                {
                    Id = m.Comment.Id,
                    CommentContent = m.Comment.CommentContent,
                    datetime = m.Comment.datetime
                }
            });
        }
    }
}