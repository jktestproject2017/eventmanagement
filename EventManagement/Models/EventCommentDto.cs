﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class EventCommentDto
    {
        public int Id { get; set; }
        public int CommentId { get; set; }
        public int EventId { get; set; }

        public virtual CommentDto Comment { get; set; }
        public virtual EventDto Event { get; set; }
    }
}