﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Foolproof;

namespace EventManagement.Models
{
    public class EventDto
    {
        public EventDto()
        {
            this.EventComments = new HashSet<EventCommentDto>();
        }    
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public System.DateTime StartDate { get; set; }
        [Required]
        [GreaterThan("StartDate")]
        public System.DateTime EndDate { get; set; }
        public string Duration { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public string AuthorId { get; set; }    
        public virtual ICollection<EventCommentDto> EventComments { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }

   }
}